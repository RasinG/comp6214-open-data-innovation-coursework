import React from 'react';
import {View} from 'react-native';
import {Provider} from 'react-redux';
import {createStore} from 'redux';
import reducers from './reducers';
import {Header} from "./components/common";
import MenuList from './components/MenuList';

const Menus = () =>{
    return (
        <Provider store = {createStore(reducers)}>
            <View style = {{flex: 1}}>
                {/*<Header headerText = "Menu"/>*/}
                <MenuList/>
            </View>
        </Provider>
    );
};

export default Menus;