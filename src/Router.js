import React from 'react';
import { Scene, Router, Actions } from 'react-native-router-flux';
import LoginForm from './components/LoginForm';
import Menu from './Menus';
import AlbumList from './components/AlbumList';
import Maps from './components/Maps';
import Success from './components/Success';

const RouterComponent = () => {
    return (
        <Router navigationBarStyle={{ backgroundColor: '#4370b7' }}>
            <Scene key = "root" hideNavBar>
            <Scene key="auth">
                <Scene key="login"
                       component={LoginForm}
                       //hideNavBar
                       navBarButtonColor = {'#FFF'}
                       title="Please Login"
                       initial
                />
            </Scene>
            <Scene key = "main">
                <Scene key="outletlist"
                       component={AlbumList}
                       navBarButtonColor = {'#FFF'}
                       title="Outlets"
                />

                <Scene key="map"
                       component={Maps}
                    ///navBarButtonColor = {'#FFF'}
                    //title="Please Login"
                    hideNavBar
                    //initial
                />


                <Scene key = "menulist"
                       component = {Menu}
                       title = "Menu"
                />
                <Scene hideNavBar
                       key = "success"
                       component = {Success}
                />
            </Scene>
            </Scene>

    </Router>
  );
};

export default RouterComponent;
