import React, { Component } from 'react';
import { Text, Image, } from 'react-native';
import { connect } from 'react-redux';
import { emailChanged, passwordChanged, loginUser } from '../actions';
import { Card, CardSection, Input, Button, Spinner } from './common';

class LoginForm extends Component{

    onEmailChange(text){
        this.props.emailChanged(text);
    }
    onPasswordChange(text) {
        this.props.passwordChanged(text);
    }

    onButtonPress() {
        const { email, password } = this.props;
        this.props.loginUser({ email, password });
    }

    renderButton() {
        if (this.props.loading) {
            return <Spinner size="large" />;
        }

        return (

                  <Button onPress={this.onButtonPress.bind(this)}>
                      Login
                  </Button>

        );
    }
    render(){
        return (
            <Card>
                <CardSection>
                    <Image
                        style = {styles.logoStyle}
                        source = {require('../assets/images/logo.png')}
                    />
                </CardSection>
                <CardSection style = {{paddingStart:20}}>
                    <Input
                        label = "Email"
                        placeholder = "test@gmail.com"
                        onChangeText={this.onEmailChange.bind(this)}
                        value={this.props.email}
                    />
                </CardSection>

                <CardSection>
                    <Input
                    secureTextEntry
                    label="Password"
                    placeholder="password"
                    onChangeText={this.onPasswordChange.bind(this)}
                    value={this.props.password}
                    />
                </CardSection>
                <Text style={styles.errorTextStyle}>
                    {this.props.error}
                </Text>
                <CardSection >
                    <Image
                        style = {styles.otherStyle}
                        source = {require('../assets/images/facebook-square.png')}
                    />
                    <Image
                        style = {styles.otherStyle}
                        source = {require('../assets/images/google-plus-square.png')}
                    />
                    <Image
                        style = {styles.otherStyle}
                        source = {require('../assets/images/twitter-square.png')}
                    />
                </CardSection>
                <CardSection>
                    {this.renderButton()}
                </CardSection>
                <Text>
                    Not registered? Just type your email and password. We can register automatically.
                </Text>
            </Card>
        );
    }
}
const styles = {
    logoStyle:{
        height: 230,
        justifyContent: 'center',
        flex: 1,
        resizeMode: 'contain'
    },
    otherStyle:{
        height: 30,
        justifyContent: 'center',
        flex: 1,
        resizeMode: 'contain'
    },
    errorTextStyle: {
    fontSize: 20,
    alignSelf: 'center',
    color: 'red'
  }
};

const mapStateToProps = ({ auth }) => {
  const { email, password, error, loading } = auth;

  return { email, password, error, loading };
};

export default connect(mapStateToProps, {
  emailChanged, passwordChanged, loginUser
})(LoginForm);
