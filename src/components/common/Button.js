import React from 'react';
import {Text, TouchableOpacity} from 'react-native';

const Button = ({onPress, children}) => {
    const {buttonStyle, textStyle} = styles;

    return(
        <TouchableOpacity
            onPress = { onPress }
            style={buttonStyle}
        >
            <Text style={textStyle}>
                {children}
            </Text>
        </TouchableOpacity>
    );
};

const styles = {
    textStyle :{
        alignSelf: 'center',
        color: '#FFF',
        fontSize: 18,
        fontWeight: '600',
        paddingTop: 10,
        paddingBottom: 10
    },
    buttonStyle:{
        flex: 1,
        alignSelf: 'stretch',
        backgroundColor: '#4050b5',
        borderRadius: 25,
        borderWidth: 5,
        borderColor: '#e9e9ef',
        marginLeft: 5,
        marginRight: 5
    }
};

export {Button};