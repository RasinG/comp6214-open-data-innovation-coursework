import React from 'react';
import {View} from 'react-native';

const CardSection = (props) => {
    return (
        <View style = {styles.containerStyles}>
            {props.children}
        </View>
    );
};

const styles = {
    containerStyles:{
        padding: 5,
        backgroundColor: '#e9e9ef',
        justifyContent: 'flex-start',
        flexDirection: 'row',
        position: 'relative'
    }
};

export {CardSection};