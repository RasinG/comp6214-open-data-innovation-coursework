import React from 'react';
import {Text, View, Image} from 'react-native';
import {Card, CardSection, Button} from './common';
import { Actions } from 'react-native-router-flux';


const AlbumDetail= ({outlets}) => {
    const {Outlets, postcode, latitude, longitude, subject, rating, photo} = outlets;
    const {
        thumbnailStyle,
        headerContentStyle,
        headerTextStyle,
        thumbnailContainerStyle,
        imageStyle,
        ratingStyle,
        ratingContainerStyle
    } = styles;
    return (
        <Card>
            <CardSection>
                <View style = {thumbnailContainerStyle}>
                    <Image
                        style = {thumbnailStyle}
                        source = {{uri: 'https://facebook.github.io/react-native/docs/assets/favicon.png'}} />
                </View>

                <View style = {headerContentStyle}>
                    <Text style = {headerTextStyle}>{Outlets}</Text>
                    <Text>{postcode}</Text>
                </View>

            </CardSection>
            <CardSection>
                <Image style = {imageStyle}
                    source = {{uri: photo}} />
            </CardSection>
            <CardSection>
                <View style = {ratingContainerStyle}>
                    <Image
                        style = {ratingStyle}
                        source = {{uri: rating}} />
                </View>
                <Button
                    onPress = {() => Actions.menulist()}
                >
                    See Menu
                </Button>
            </CardSection>
        </Card>
    );
};

const styles = {
    headerContentStyle:{
        flexDirection: 'column',
        justifyContent: 'space-between'
    },
    headerTextStyle:{
        fontSize: 18
    },
    thumbnailStyle:{
        height: 50,
        width: 50
    },
    thumbnailContainerStyle:{
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 10,
        marginRight: 10
    },
    imageStyle: {
        justifyContent: 'center',
        height: 300,
        flex: 1,
        width: null,
    },
    ratingStyle: {
        height: 50,
        width: 100
    },
    ratingContainerStyle:{
        justifyContent: 'flex-end',
        alignItems: 'flex-end'
    }
};
export default AlbumDetail;