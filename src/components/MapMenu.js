import React, {Component} from 'react';
import { StyleSheet, View, ScrollView} from 'react-native';
import Mapbox from '@mapbox/react-native-mapbox-gl';
import axios from 'axios';
import {Icon, Card, ListItem, Button, Overlay, Text} from 'react-native-elements';
import {Actions} from 'react-native-router-flux';

class MapMenu extends Component {
    state = {
        shops: []
    };

    componentWillMount() {
        axios.get('https://api.myjson.com/bins/19o2t2')
        .then(response => this.setState({shops: response.data}));
    }

    renderMapMenu() {
        return this.state.shops.map(shops => {
            return (
                <Card
                key = {shops.id}
                title= {shops.properties.name}
                image={{uri: shops.properties.photo}}>
                    <View style={styles.uniButton}>
                        <Icon
                            name='map'
                            color='#3F51B5'
                            size={12}/>
                        <Text style={styles.textStyles}>
                             {shops.properties.addressFormatted}
                        </Text>
                    </View>

                    <View style={styles.uniButton}>
                        <Icon
                            name='timelapse'
                            color='#3F51B5'
                            size={12}/>
                        <Text style={styles.textStyles}>
                             {shops.properties.hoursFormatted}
                        </Text>
                    </View>
                    <View style={styles.uniButton}>
                        <Icon
                            name='phone'
                            color='#3F51B5'
                            size={12}/>
                        <Text style={styles.textStyles}>
                             {shops.properties.phoneFormatted}
                        </Text>
                    </View>
                    <Button
                    onPress = {() => Actions.menulist()}
                    icon={<Icon name='store' color='#ffffff' />}
                    backgroundColor='#3F51B5'
                    fontFamily='Lato'
                    buttonStyle={styles.buttonStyles}
                    title='Order Now' />
                </Card>
            );
        }

        );
    }

    render() {
        return (
        <View>
            {this.renderMapMenu()}
        </View>
        );
    }
}

const styles = StyleSheet.create({
    textStyles: {
        marginBottom: 0,
        marginLeft: 3,
        marginRight: 3,
        fontSize: 10
    },
    buttonStyles: {
       borderRadius: 0,
       marginLeft: 0,
       marginRight: 0,
       marginBottom: 0,
       marginTop: 10
    },
    uniButton: {
         flexDirection: 'row'
    }
});

export default MapMenu;