import React from 'react';
import {Text, View, Image, ScrollView} from 'react-native';
import {CardSection, Card, Button} from "./common";
import QRCode from 'react-native-qrcode';

import { Actions } from 'react-native-router-flux';
const Success = () =>{
    return (
        <ScrollView>
            <View style = {styles.headerContainer}>
                <Text style = {styles.headerStyle}>Congratulations!</Text>
                <Text style = {styles.normalStyle}>Your order is placed!</Text>
            </View>

            <View style = {styles.headerContainer}>
            {/*<Image*/}
                {/*style = {styles.logoStyle}*/}
                {/*source = {require('../assets/images/orderSuccess.png')}*/}
            {/*/>*/}
            </View>

            <View style = {styles.headerContainer}>
                <Text style = {styles.normalStyle}>Your waiting code is:</Text>
                <QRCode
                    value={"Order Success QED1832"}
                    size={200}
                    bgColor='black'
                    fgColor='white'/>
                <Text style = {styles.normalStyle}>Thank you!</Text>

            </View>

            <Card>
                <CardSection>
                <Button onPress = {() => Actions.outletlist} >
                    Go back
                </Button>
                </CardSection>
            </Card>
        </ScrollView>
    );
};

const styles = {
    headerContainer:{
        justifyContent: 'center',
        alignSelf: 'center',
        flexDirection: 'column',
        position: 'relative'
    },
    headerStyle: {
        marginTop: 30,
        marginBottom: 15,
        fontSize: 20,
        alignSelf: 'center',
        color: '#87ceeb'
    },
    normalStyle:{
        marginTop: 10,
        marginBottom: 10,
        fontSize: 18,
        alignSelf: 'center',
        color: '#23556d'
    },

    imageStyle: {
        justifyContent: 'center',
        alignSelf: 'center',
        height: 150,
        flex: 1,
        width: null,
    }
}

export default Success;
