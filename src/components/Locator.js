import React, {Component} from 'react';
import { StyleSheet, View, Text, ScrollView} from 'react-native';
import MapboxGL from '@mapbox/react-native-mapbox-gl';
import StoreLocatorKit from '@mapbox/store-locator-react-native';
import places from '../assets/Places.json';
import { theme } from './Theme';

MapboxGL.setAccessToken('pk.eyJ1IjoicmFzaW5ndWUiLCJhIjoiY2pndG1rZXkxMHU1dTJ5bWFob3FhYWhhZiJ9.Bu3WF2ZIfD81iRxgHR2b2g');

class Locator extends Component {
    state = {
    initialLocation: [-1.3890, 50.9322]};

    render() {
        return (
            <View>
                <StoreLocatorKit.MapView
                      simulateUserLocation
                      accessToken={'pk.eyJ1IjoicmFzaW5ndWUiLCJhIjoiY2pndG1rZXkxMHU1dTJ5bWFob3FhYWhhZiJ9.Bu3WF2ZIfD81iRxgHR2b2g'}
                      theme={theme}
                      centerCoordinate={this.state.initialLocation}
                      featureCollection={places}
                      zoomLevel={13}
                      style={styles.matchParent} />
            </View>
        );
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

});

export default Locator;