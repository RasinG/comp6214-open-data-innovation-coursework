import MapboxGL from '@mapbox/react-native-mapbox-gl';
import StoreLocatorKit from '@mapbox/store-locator-react-native';

import reallyCoolIcon from '../assets/images/white_unselected_house.png';
import evenCoolerIcon from '../assets/images/gray_selected_house.png';
import ultraCoolIcon from '../assets/images/house_icon.png';

export const theme = new StoreLocatorKit.Theme({
  icon: reallyCoolIcon,
  activeIcon: evenCoolerIcon,
  styleURL: MapboxGL.StyleURL.Light,
  primaryColor: `#A35BCD`,
  primaryDarkColor: '#5D39BA',
  directionsLineColor: '#987DDF',
  cardIcon: ultraCoolIcon,
  cardTextColor: '#6A159B',
  accentColor: '#C7A8D9',
});