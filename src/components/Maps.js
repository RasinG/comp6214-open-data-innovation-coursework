import React, {Component} from 'react';
import { StyleSheet, View, ScrollView} from 'react-native';
import {Icon, Card, ListItem, Button, Overlay, Text} from 'react-native-elements'
import Mapbox from '@mapbox/react-native-mapbox-gl';
import axios from 'axios';
import MapsAnnotations from './MapsAnnotations';
import MapMenu from './MapMenu';
import {Actions} from 'react-native-router-flux';

Mapbox.setAccessToken('pk.eyJ1IjoicmFzaW5ndWUiLCJhIjoiY2pndG1rZXkxMHU1dTJ5bWFob3FhYWhhZiJ9.Bu3WF2ZIfD81iRxgHR2b2g');

class Maps extends Component {
    state = { shops: [],
    location: [],
    isVisible: false
    };

    componentWillMount() {
        axios.get('http://rasinwp.qiniudn.com/20restaurant.json')
        .then(response => this.setState({shops: response.data}));
    }



  //渲染地图标记点
  renderAnnotations() {
   return this.state.shops.map(shops =>
        <MapsAnnotations key={shops.subject} shops = {shops}/>
    );
  }

  //渲染商店列表
  renderMapMenu() {
    return this.state.shops.map(shops =>
        <MapMenu key={shops.subject} Outlets={shops.Outlets}/>
    );
  }
  //定位到自己
  centerMap () {
     navigator.geolocation.getCurrentPosition(
     (position) => {
        this._map.setCamera({
          centerCoordinate: [position.coords.longitude, position.coords.latitude],
          });
        },
     );
  }

  //显示菜单
  displayMenu () {
    this.setState({isVisible: true});
  }

  //Main screen
  render() {
    return (
      <View style={styles.container}>
        <Mapbox.MapView
        ref={map => { this._map = map; }}
        styleURL={Mapbox.StyleURL.Street}
        zoomLevel={12}
        compassEnabled = {true}
        centerCoordinate={[-1.400, 50.943]}
        style={styles.container}
        showUserLocation={true}
        onUpdateUserLocation={this.onUpdateUserLocation}>
            {this.renderAnnotations()}
        </Mapbox.MapView>

        <Button
           onPress={() => this.centerMap()}
           title="Find Me"
           color="white"
           buttonStyle={styles.locateUser}
           />

        <Button
           onPress={() => this.displayMenu()}
           title="Explore"
           color="white"
           buttonStyle={styles.explore}
           />

           <Overlay
           isVisible={this.state.isVisible}
           windowBackgroundColor="rgba(70, 70, 70, .5)"
           overlayBackgroundColor="#E0E0E0"
           width={300}
           height={500}
           onBackdropPress={() => this.setState({isVisible: false})}>
               <ScrollView>
                    <MapMenu></MapMenu>
               </ScrollView>
           </Overlay>

      </View>

      //<Text>{this.state.location.coords.latitude}</Text>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  locateUser: {
    backgroundColor: "#3F51B5",
    position: 'absolute',
    bottom: 600,
    right:20,
    width: 83,
    height: 30,
    borderColor: "transparent",
    alignItems: 'center',
    //justifyContent: 'center',
    borderWidth: 0,
    borderRadius: 3
  },
  explore: {
    backgroundColor: "#3F51B5",
    position: 'absolute',
    bottom: 600,
    right:120,
    width: 83,
    height: 30,
    borderColor: "transparent",
    alignItems: 'center',
    //justifyContent: 'center',
    borderWidth: 0,
    borderRadius: 3
  },
  cardContainer: {
    position: 'absolute',
    bottom: 0,
    height: 0,
  }
});

export default Maps;