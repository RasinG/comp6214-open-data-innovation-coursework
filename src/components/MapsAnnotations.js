import React, {Component} from 'react';
import { StyleSheet, View, Text} from 'react-native';
import Mapbox from '@mapbox/react-native-mapbox-gl';

const MapsAnnotations = ({shops}) => {
    const {subject, Outlets, Latitude, longitude} = shops;
     const {
        annotationContainer,
        annotationFill
    } = styles;
    return (
        <Mapbox.PointAnnotation
                id = {Outlets}
                coordinate = {[longitude, Latitude]}
                >
                <View style={styles.annotationContainer}>
                    <View style={styles.annotationFill}/>
                </View>
                <Mapbox.Callout title= {Outlets} />
        </Mapbox.PointAnnotation>
    )
}

const styles = {
    annotationContainer: {
        width: 20,
        height: 20,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'white',
        borderRadius: 15,
      },
      annotationFill: {
        width: 20,
        height: 20,
        borderRadius: 15,
        backgroundColor: 'orange',
        transform: [{scale: 0.6}],
      }
}

export default MapsAnnotations;