import React, {Component} from 'react';
import {Text, TouchableWithoutFeedback, TouchableOpacity, View, Image, LayoutAnimation} from 'react-native';
import {connect} from 'react-redux';
import {CardSection, Card, Button} from './common';
import * as actions from '../actions';
import {Actions} from "react-native-router-flux";
import {Icon} from 'react-native-elements'

class ListItem extends Component{
    constructor(props) {
        super(props)
        this.state = { count: 0 }
    }

    componentWillUpdate(){
        LayoutAnimation.spring()
    }

  onPlus = () => {
    this.setState({
      count: this.state.count+1
    });
  }

  onSubtract = () => {
      this.setState({
        count: this.state.count- 1
      });
  }

    renderMenu(){
        const {imageStyle, headerContentStyle, headerTextStyle} = styles;
        const {library, selectedLibraryId} = this.props;
        const menu = library.menu;
        if (library.subject === selectedLibraryId){
            return (
                <Card>
                    <CardSection>
                        <Image style = {imageStyle}
                               source = {{uri: library.photo}} />
                    </CardSection>
                    <View>
                        {menu.map(item =>
                        <View  key={item.name}>
                            { item.available ?
                                <View style = {{ justifyContent:'space-between', flexDirection: 'row'}}>
                                <Text style={{flex: 1, color:"#000", fontSize: 15}}>{item.name}</Text>
                                <Text style={{flex: 1, color:"#000", fontSize: 15}}>{item.price}</Text>
                                <TouchableOpacity
                                  style={styles.plusButton}
                                  onPress={this.onSubtract}
                                >
                                     <Text style={styles.plusText}>-</Text>
                                </TouchableOpacity>
                                <Text style={{flex: 1, color:"#000", fontSize: 20, textAlign: 'center'}}>{this.state.count}</Text>
                                <TouchableOpacity
                                       style={styles.plusButton}
                                       onPress={this.onPlus}
                                     >
                                       <Text style={styles.plusText}>+</Text>
                                </TouchableOpacity>
                                </View>
                                :
                                <View style = {{ justifyContent:'space-between', flexDirection: 'row'}}>
                                <Text style={{flex: 1, color:"#CCC", fontSize: 15}}>{item.name}</Text>
                                <Text style={{flex: 1, color:"#CCC", fontSize: 15}}>{item.price}</Text>
                                <TouchableOpacity
                                  style={styles.plusButtonAlt}
                                  onPress={this.onSubtract}
                                >
                                     <Text style={styles.plusTextAlt}>-</Text>
                                </TouchableOpacity>
                                <Text style={{flex: 1, color:"#000", fontSize: 20, textAlign: 'center'}}>{this.state.count}</Text>
                                <TouchableOpacity
                                       style={styles.plusButtonAlt}
                                       onPress={this.onPlus}
                                     >
                                       <Text style={styles.plusTextAlt}>+</Text>
                                </TouchableOpacity>
                                </View>
                            }
                        </View>)}
                    </View>
                    <Button onPress = {() => Actions.success()}>
                        Confirm
                    </Button>
                </Card>
            );
        }
    }

    render(){
        const {titleStyle} = styles;
        const {subject, outlets, menu} = this.props.library;


        return(
            <TouchableWithoutFeedback
                onPress = {() =>this.props.selectLibrary(subject)}
            >
                <View>
                    <CardSection>
                        <Text style = {titleStyle}>{outlets}</Text>
                    </CardSection>
                        {this.renderMenu()}
                </View>
            </TouchableWithoutFeedback>
        );
    }
}

const styles = {
    titleStyle: {
        color: '#000',
        fontSize: 18,
        paddingLeft: 15
    },
    headerContentStyle:{
        flexDirection: 'column',
        justifyContent: 'space-between'
    },
    headerTextStyle:{
        fontSize: 18
    },
    imageStyle: {
        justifyContent: 'center',
        height: 300,
        flex: 1,
        width: null,
    },
    plusButton: {
        borderRadius: 15,
        height: 30,
        width: 30,
        alignItems: 'center',
        backgroundColor: '#3F51B5',
        padding: 0
    },
    plusText: {
       fontSize: 20,
       color: 'white',
       fontWeight: 'bold'
    },
    plusButtonAlt: {
            borderRadius: 15,
            height: 30,
            width: 30,
            alignItems: 'center',
            backgroundColor: '#B1B1B1',
            padding: 0
        },
        plusTextAlt: {
           fontSize: 20,
           color: 'white',
           fontWeight: 'bold'
        },
};

const mapStateToProps = state =>{
    return {selectedLibraryId: state.selectedLibraryId}
};

export default connect(mapStateToProps, actions)(ListItem);
