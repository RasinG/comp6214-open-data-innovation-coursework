import {combineReducers} from 'redux';
import MenuReducer from './MenuReducer';
import SelectionReducer from './SelectionReducer';
import AuthReducer from './AuthReducer';

export default combineReducers({
    auth: AuthReducer,
    libraries: MenuReducer,
    selectedLibraryId: SelectionReducer
});


