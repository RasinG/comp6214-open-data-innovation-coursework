import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import firebase from 'firebase';
import ReduxThunk from 'redux-thunk';
import reducers from './reducers';
import Router from './Router';

class Login extends Component{

    componentWillMount() {
        firebase.initializeApp({
            apiKey: "AIzaSyBtpBWuPYz5YxxvLkHTOfhmGOvXgiYZlQE",
            authDomain: "opendataapp-f32e3.firebaseapp.com",
            databaseURL: "https://opendataapp-f32e3.firebaseio.com",
            projectId: "opendataapp-f32e3",
            storageBucket: "opendataapp-f32e3.appspot.com",
            messagingSenderId: "278586115572"
        });
    }


    render(){
        const store = createStore(reducers, {}, applyMiddleware(ReduxThunk));

        return (
            <Provider store={store}>
                <Router />
            </Provider>
        );
    }
}

export default Login;